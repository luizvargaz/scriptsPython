import os
import pandas as pd
import openpyxl

# write the variables
output_files_folder = 'output_csv'

# set the file name and location
directory_data = 'C:/Users/LVARGAS/Documents/CIMMYT/limpieza_BEM_python'
file_name = 'EXPORTAR_COMPLETA_2018.xlsx'
file = directory_data + '//' + file_name

## set the working directory
wd = os.getcwd()
print('\n','The work directory is:',wd, '\n')

## open the Excel Workbook
wb = openpyxl.load_workbook(filename = file, read_only=True)
print(wb.sheetnames, '\n')

sheetnames = wb.sheetnames

for sheet in sheetnames:
    if sheet == 'MasAgro':
        continue
    else:
        print('working in the sheet: ' +sheet, '\n')
        dataFrame = wb[hoja]
        data = dataFrame.values
        ## get the first line in file as a header line
        columns = next(data)[0:]
        ## create a DataFrame based on the second and subsequent lines of data
        df = pd.DataFrame(data, columns = columns)

        # create the folder to the output files
        directorio_output_files_folder = directory_data + '\\' + output_files_folder
        if not os.path.exists(directorio_output_files_folder):
            os.makedirs(directorio_output_files_folder)
            print('Directory created')

        # name and create the output file
        nombreArchivo = directorio_output_files_folder + '\\' + sheet + ' .csv'

        df.to_csv(nombreArchivo, encoding = 'windows-1252', index = False)
